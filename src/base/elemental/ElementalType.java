package base.elemental;

public enum ElementalType {
	Normal, Acid, Cold, Electric, Fire, Falling, Fatigue;
}
