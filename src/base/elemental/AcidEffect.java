package base.elemental;

import base.Character;
import base.DamageType;
import base.Game;
import base.Time;
import base.TimeEffect;

public class AcidEffect extends TimeEffect {
	
	int			damage;
	Character	target;
	
	public AcidEffect(int damage, Character target) {
		super(Game.time.getSeconds() + Time.Round.getSeconds());
		this.damage = damage;
		this.target = target;
		Game.effect(this);
	}
	
	@Override
	public void activate() {
		damage -= 1;
		target.resist(damage, DamageType.PHYSICAL, ElementalType.Acid, 0);
		target.reduceArmor(1);
		if (damage > 1) {
			this.add(Time.Round);
			Game.effect(this);
		}
	}
}
