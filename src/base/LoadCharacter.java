package base;

import java.io.File;

import javax.swing.JFileChooser;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import base.exceptions.IllegalAttributeException;
import base.exceptions.IllegalMetatypeException;
import base.exceptions.IllegalSkillException;

public class LoadCharacter {
	
	public static void main(String[] args) {
		boolean nice = false;
		if (nice) {
			JFileChooser fileChooser = new JFileChooser();
			fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
			fileChooser.setCurrentDirectory(
					new File("/home/joey/Documents/Programmieren/Java Workspace/sr5simjava/raw/"));
			int result = fileChooser.showOpenDialog(null);
			if (result == JFileChooser.APPROVE_OPTION) {
				File selectedFile = fileChooser.getSelectedFile();
				System.out.println("Selected file: " + selectedFile.getAbsolutePath());
				load(selectedFile);
			}
		} else {
			
			Character c = load(
					new File("/home/joey/Documents/Projekte/SR5/NanoCyber/NanoCyber.chum5"));
			System.out.println(c);
		}
	}
	
	private static Object eval(String str, Document doc, QName con) {
		try {
			expr = xpath.compile(str);
			return expr.evaluate(doc, con);
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private static String eval(String str, Document doc) {
		try {
			expr = xpath.compile(str);
			return expr.evaluate(doc);
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private static XPathFactory		xpFactory	= XPathFactory.newInstance();
	private static XPath			xpath		= xpFactory.newXPath();
	private static XPathExpression	expr;
	
	public static Character load(File file) {
		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(file);
			
			doc.getDocumentElement().normalize();
			
			Metatype metatype = toMetatype(eval("character/metatype", doc));
			String name = eval("character/name", doc);
			String alias = eval("character/alias", doc);
			String sex = eval("character/sex", doc);
			String age = eval("character/age", doc);
			String eyes = eval("character/eyes", doc);
			String height = eval("character/height", doc);
			String weight = eval("character/weight", doc);
			String skin = eval("character/skin", doc);
			String hair = eval("character/hair", doc);
			String description = eval("character/description", doc);
			String background = eval("character/background", doc);
			String concept = eval("character/concept", doc);
			String notes = eval("character/notes", doc);
			String gamenotes = eval("chatacter/gamenotes", doc);
			String playername = eval("character/playername", doc);
			double essence = Double.parseDouble(eval("character/totaless", doc));
			// TODO Mage, Rigger, Technomancerstuff
			Character c = new Character(name, alias, metatype, sex, age, eyes, height, weight, skin,
					hair, description, background, concept, notes, gamenotes, playername);
			
			// ---------------- Load Attributes -------------------
			NodeList nList = (NodeList) eval("character/attributes/attribute", doc,
					XPathConstants.NODESET);
			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node node = nList.item(temp);
				NodeList n2 = node.getChildNodes();
				String attribute = "";
				try {
					attribute = n2.item(1).getTextContent();
					Attribute attr = toAttribute(attribute);
					int metatypemin = Integer.parseInt(n2.item(3).getTextContent());
					int metatypemax = Integer.parseInt(n2.item(5).getTextContent());
					// int metatypeaugmax = Integer.parseInt(n2.item(7).getTextContent());
					int base = Integer.parseInt(n2.item(9).getTextContent());
					int karma = Integer.parseInt(n2.item(11).getTextContent());
					// String category = n2.item(13).getTextContent();
					// int totalvalue = Integer.parseInt(n2.item(15).getTextContent());
					
					// check correct metatype values
					if (attr == Attribute.ESS) {
						double lost = metatype.baseStats.get(Attribute.ESS) - essence;
						c.setEssence(lost);
					} else if (attr == Attribute.RES) {
						base = metatypemin;
						c.setAttribute(attr, base + karma);
					} else if (metatypemin != metatype.baseStats.get(attr)
							|| metatypemax != metatype.maxStats.get(attr)) {
						System.err.println("Limiterror (" + attr + "): Metatype Limit ("
								+ metatype.baseStats.get(attr) + ", " + metatype.maxStats.get(attr)
								+ ") does not match given Limit (" + metatypemin + ", "
								+ metatypemax + ")");
						c.setAttribute(attr, base + karma);
					} else {
						c.setAttribute(attr, base + karma);
					}
				} catch (IllegalAttributeException e) {
					System.out.println(e.getMessage());
				}
			}
			System.out.println("Essence: " + c.getEssence());
			
			// ------------------ Load Skills ----------------
			NodeList skillNames = (NodeList) eval("character/newskills/skills/skill/name", doc,
					XPathConstants.NODESET);
			NodeList skillBases = (NodeList) eval("character/newskills/skills/skill/base", doc,
					XPathConstants.NODESET);
			NodeList skillKarmas = (NodeList) eval("character/newskills/skills/skill/karma", doc,
					XPathConstants.NODESET);
			for (int temp = 0; temp < skillNames.getLength(); temp++) {
				String skillName = skillNames.item(temp).getTextContent();
				String skillBase = skillBases.item(temp).getTextContent();
				String skillKarma = skillKarmas.item(temp).getTextContent();
				try {
					Skill skill = toSkill(skillName);
					int base = Integer.parseInt(skillBase);
					int karma = Integer.parseInt(skillKarma);
					if (base + karma != 0) {
						System.out.println(skill + ": " + (base + karma));
						c.setSkill(skill, base + karma);
					}
				} catch (IllegalSkillException e) {
					System.out.println(e.getMessage());
				}
			}
			return c;
			
		} catch (IllegalMetatypeException e) {
			System.out.println(e.getMessage());
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
		return null;
	}
	
	private static Metatype toMetatype(String str) {
		switch (str.toLowerCase()) {
			case "human":
				return Metatype.Human;
			case "elf":
				return Metatype.Elf;
			case "dwarf":
				return Metatype.Dwarf;
			case "ork":
				return Metatype.Ork;
			case "troll":
				return Metatype.Troll;
			default:
				throw new IllegalMetatypeException("Unknown Metatype: " + str);
		}
	}
	
	private static Attribute toAttribute(String str) {
		switch (str.toLowerCase()) {
			case "bod":
				return Attribute.BOD;
			case "agi":
				return Attribute.AGI;
			case "rea":
				return Attribute.REA;
			case "str":
				return Attribute.STR;
			case "cha":
				return Attribute.CHA;
			case "int":
				return Attribute.INT;
			case "log":
				return Attribute.LOG;
			case "wil":
				return Attribute.WIL;
			case "edg":
				return Attribute.EDG;
			case "mag":
				return Attribute.MAG;
			case "res":
				return Attribute.RES;
			case "ess":
				return Attribute.ESS;
			default:
				throw new IllegalAttributeException("Unknown Attribute: " + str);
		}
	}
	
	private static Skill toSkill(String str) {
		switch (str.toLowerCase()) {
			case "gymnastics":
				return Skill.Gymnastics;
			case "arcana":
				return Skill.Arcana;
			case "biotechnology":
				return Skill.Biotechnology;
			case "pilot ground craft":
				return Skill.PilotGroundCraft;
			case "chemistry":
				return Skill.Chemistry;
			case "computer":
				return Skill.Computer;
			case "decompiling":
				return Skill.Decompiling;
			case "intimidation":
				return Skill.Intimidation;
			case "electronic warfare":
				return Skill.ElectronicWarfare;
			case "escape artist":
				return Skill.EscapeArtist;
			case "first aid":
				return Skill.FirstAid;
			case "automotive mechanic":
				return Skill.AutomotiveMechanic;
			case "palming":
				return Skill.Palming;
			case "flight":
				return null; // TODO correct?
			case "pilot aircraft":
				return Skill.PilotAircraft;
			case "free-fall":
				return Skill.FreeFall;
			case "forgery":
				return Skill.Forgery;
			case "leadership":
				return Skill.Leadership;
			case "etiquette":
				return Skill.Etiquette;
			case "gunnery":
				return Skill.Gunnery;
			case "longarms":
				return Skill.Longarms;
			case "hacking":
				return Skill.Hacking;
			case "artisan":
				return Skill.Artisan;
			case "hardware":
				return Skill.Hardware;
			case "industrial mechanic":
				return Skill.IndustrialMechanic;
			case "blades":
				return Skill.Blades;
			case "clubs":
				return Skill.Clubs;
			case "compiling":
				return Skill.Compiling;
			case "cybertechnology":
				return Skill.Cybertechnology;
			case "running":
				return Skill.Running;
			case "aeronautics mechanic":
				return Skill.AeronauticsMechanic;
			case "pilot walker":
				return Skill.PilotWalker;
			case "cybercombat":
				return Skill.Cybercombat;
			case "medicine":
				return Skill.Medicine;
			case "navigation":
				return Skill.Navigation;
			case "pistols":
				return Skill.Pistols;
			case "archery":
				return Skill.Archery;
			case "pilot aerospace":
				return Skill.PilotAerospace;
			case "registering":
				return Skill.Registering;
			case "pilot watercraft":
				return Skill.PilotWatercraft;
			case "sneaking":
				return Skill.Sneaking;
			case "locksmith":
				return Skill.Locksmith;
			case "automatics":
				return Skill.Automatics;
			case "heavy weapons":
				return Skill.HeavyWeapons;
			case "swimming":
				return Skill.Swimming;
			case "nautical mechanic":
				return Skill.NauticalMechanic;
			case "software":
				return Skill.Software;
			case "demolitions":
				return Skill.Demolitions;
			case "tracking":
				return Skill.Tracking;
			case "survival":
				return Skill.Survival;
			case "diving":
				return Skill.Diving;
			case "animal handling":
				return Skill.AnimalHandling;
			case "instruction":
				return Skill.Instruction;
			case "negotiation":
				return Skill.Negotiation;
			case "disguise":
				return Skill.Disguise;
			case "impersonation":
				return Skill.Impersonation;
			case "performance":
				return Skill.Performance;
			case "armorer":
				return Skill.Armorer;
			case "unarmed combat":
				return Skill.UnarmedCombat;
			case "perception":
				return Skill.Perception;
			case "throwing weapons":
				return Skill.ThrowingWeapons;
			case "con":
				return Skill.Con;
			
			default:
				throw new IllegalSkillException("Unknown Skill: " + str);
		}
	}
}
