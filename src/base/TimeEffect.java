package base;

public abstract class TimeEffect extends Time implements Effect {
	
	public TimeEffect(Time time) {
		super(time);
	}
	
	public TimeEffect(int seconds) {
		super(seconds);
	}
	
	public TimeEffect(int seconds, int initiative) {
		super(seconds, initiative);
	}
	
}
