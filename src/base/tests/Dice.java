package base.tests;

import java.util.Random;

import base.exceptions.InvalidEdgeUseException;

public class Dice {
	
	public final static int	defaultSides	= 6;
	public final static int	FAIL			= 1;
	public final static int	WIN				= 5;
	public final static int	EXP				= 6;
	
	private int				dice;
	private int				sides;
	private int				wins, fails, exps;
	private boolean			glitch, critical, edged;
	
	public Dice(int count) {
		this(count, defaultSides, false);
	}
	
	public Dice(int count, boolean explode) {
		this(count, defaultSides, explode);
	}
	
	public Dice(int count, int sides, boolean explode) {
		this.sides = sides;
		this.edged = explode;
		
		if (explode) {
			roll(count);
			explode(exps);
		} else {
			roll(count);
		}
		evaluate(true);
	}
	
	private void explode(int amount) {
		exps = 0;
		roll(amount);
		if (exps > 0)
			explode(exps);
	}
	
	public void preEdge(int bonus) {
		if (edged)
			throw new InvalidEdgeUseException("Already used edge once in this throw!");
		edged = true;
		explode(bonus);
		evaluate(true);
	}
	
	public void postEdge() {
		if (edged)
			throw new InvalidEdgeUseException("Already used edge once in this throw!");
		edged = true;
		roll(dice - wins);
		evaluate(false);
	}
	
	public void negateGlitch() {
		if (edged)
			throw new InvalidEdgeUseException("Already used edge once in this throw!");
		edged = true;
		if (critical)
			critical = false;
		else
			glitch = false;
	}
	
	private void roll(int amount) {
		amount = Math.max(amount, 0);
		dice += amount;
		Random r = new Random();
		for (int i = 0; i < amount; i++) {
			int rand = r.nextInt(sides) + 1;
			if (rand <= FAIL)
				fails++;
			if (rand >= WIN)
				wins++;
			if (rand == EXP)
				exps++;
		}
	}
	
	private void evaluate(boolean override) {
		glitch = override ? fails >= dice / 2. : glitch || fails >= dice / 2.;
		critical = override ? glitch && wins == 0 : critical || (glitch && wins == 0);
	}
	
	public void limit(int limit) {
		if (wins > limit)
			wins = limit;
	}
	
	public int getDice() {
		return dice;
	}
	
	public int getSides() {
		return sides;
	}
	
	public int getWins() {
		return wins;
	}
	
	public int getFails() {
		return fails;
	}
	
	public boolean isGlitch() {
		return glitch;
	}
	
	public boolean isCritical() {
		return critical;
	}
	
	public boolean edged() {
		return edged;
	}
	
	public String toString() {
		return dice + "W" + sides + "; wins=" + wins + ", fail=" + fails
				+ (isGlitch() ? (isCritical() ? " [Critical]" : " [Glitch]") : "");
	}
	
	public static int value(int count) {
		return value(count, defaultSides);
	}
	
	public static int value(int count, int side) {
		Random rand = new Random();
		int sum = 0;
		for (int i = 0; i < count; i++) {
			sum += rand.nextInt(side) + 1;
		}
		return sum;
	}
}
