package base.tests;

import java.util.ArrayList;
import java.util.Arrays;

import base.Attribute;
import base.Character;
import base.Game;
import base.Skill;
import base.SkillSpecialty;

public class OpposedTest {
	
	public static class Builder {
		private Character					c1, c2;
		private int							d1, d2;
		private int							limit		= -1;
		private ArrayList<SkillSpecialty>	special1	= new ArrayList<SkillSpecialty>();
		private ArrayList<SkillSpecialty>	special2	= new ArrayList<SkillSpecialty>();
		
		public Builder(Character attacker, Character defender) {
			this.c1 = attacker;
			this.c2 = defender;
		}
		
		public Builder addAtk(int bonus) {
			this.d1 += bonus;
			return this;
		}
		
		public Builder addAtk(Attribute attribute) {
			addAtk(c1.getAttribute(attribute));
			return this;
		}
		
		public Builder addAtk(Skill skill) {
			addAtk(c1.getSkill(skill));
			special1.addAll(Arrays.asList(c1.getSpecials(skill)));
			return this;
		}
		
		public Builder addAtk(Attribute attr1, Attribute attr2) {
			addAtk(attr1);
			addAtk(attr2);
			return this;
		}
		
		public Builder addAtk(Skill skill, Attribute attr) {
			addAtk(skill);
			addAtk(attr);
			return this;
		}
		
		public Builder mali() {
			this.d1 -= c1.mali();
			return this;
		}
		
		public Builder limit(int limit) {
			this.limit = limit;
			return this;
		}
		
		public Builder addDef(int bonus) {
			this.d2 += bonus;
			return this;
		}
		
		public Builder addDef(Attribute attribute) {
			addDef(c2.getAttribute(attribute));
			return this;
		}
		
		public Builder addDef(Skill skill) {
			addDef(c2.getSkill(skill));
			special2.addAll(Arrays.asList(c2.getSpecials(skill)));
			return this;
		}
		
		public Builder addDef(Attribute attr1, Attribute attr2) {
			addDef(attr1);
			addDef(attr2);
			return this;
		}
		
		public Builder addDef(Skill skill, Attribute attr) {
			addDef(skill);
			addDef(attr);
			return this;
		}
		
		public OpposedTest build() {
			return new OpposedTest(c1, d1, limit, (SkillSpecialty[]) special1.toArray(), c2, d2,
					(SkillSpecialty[]) special2.toArray());
		}
	}
	
	public Dice	attacker, defender;
	Character	c1, c2;
	private int	limit;
	
	public OpposedTest(Character c1, Skill skill1, Attribute attr1, int lim, Character c2,
			Attribute attr2, Skill skill2) {
		this(c1, c1.getSkill(skill1) + c1.getAttribute(attr1), lim, c1.getSpecials(skill1), c2,
				c2.getSkill(skill2) + c2.getAttribute(attr2), c2.getSpecials(skill2));
	}
	
	public OpposedTest(Character c1, Skill skill1, Attribute attr1, int bonus, int lim,
			Character c2, Attribute attr2, Skill skill2) {
		this(c1, c1.getSkill(skill1) + c1.getAttribute(attr1) + bonus, lim, c1.getSpecials(skill1),
				c2, c2.getSkill(skill2) + c2.getAttribute(attr2), c2.getSpecials(skill2));
	}
	
	public OpposedTest(Character c1, Attribute attr1a, Attribute attr1b, int lim, Character c2,
			Attribute attr2a, Attribute attr2b) {
		this(c1, c1.getAttribute(attr1a) + c1.getAttribute(attr1b), lim, null, c2,
				c2.getAttribute(attr2a) + c2.getAttribute(attr2b), null);
	}
	
	public OpposedTest(Character c1, Attribute attr1a, Attribute attr1b, int bonus, int lim,
			Character c2, Attribute attr2a, Attribute attr2b) {
		this(c1, c1.getAttribute(attr1a) + c1.getAttribute(attr1b) + bonus, lim, null, c2,
				c2.getAttribute(attr2a) + c2.getAttribute(attr2b), null);
	}
	
	public OpposedTest(Character c1, int dice1, int lim, SkillSpecialty[] specials1, Character c2,
			int dice2, SkillSpecialty[] specials2) {
		this.c1 = c1;
		this.c2 = c2;
		
		boolean edge1 = false, edge2 = false;
		
		if (Game.interactive && specials1 != null) {
			Game.msg("Specialties Attacker:");
			for (int i = 0; i < specials1.length; i++) {
				Game.msg(" - " + specials1[i].toString());
			}
			if (Game.askBoolean("Attacker: Any valid specialty?"))
				dice1 += 2;
		}
		
		if (Game.interactive && specials2 != null) {
			Game.msg("Specialties Defender:");
			for (int i = 0; i < specials2.length; i++) {
				Game.msg(" - " + specials2[i].toString());
			}
			if (Game.askBoolean("Defender: Any valid specialty?"))
				dice2 += 2;
		}
		
		if (Game.interactive) // preEdge
			edge1 = Game.askBoolean("Attacker: Push the limits?");
		
		if (Game.interactive) // preEdge
			edge2 = Game.askBoolean("Defender: Push the limits?");
		
		this.limit = (edge1 || limit <= 0) ? Integer.MAX_VALUE : lim;
		attacker = new Dice(dice1 + (edge1 ? c1.getAttribute(Attribute.EDG) : 0), edge1);
		defender = new Dice(dice2 + (edge2 ? c2.getAttribute(Attribute.EDG) : 0), edge2);
		
		if (Game.interactive && !attacker.edged() && attacker.isGlitch()) // negate critical
			if (Game.askBoolean("Attacker: A " + (attacker.isCritical() ? "critical" : "")
					+ " glitch has occured! Use edge to negate this?"))
				negateGlitchAttacker();
			
		if (Game.interactive && !defender.edged() && defender.isGlitch()) // negate critical
			if (Game.askBoolean("Defender: A " + (defender.isCritical() ? "critical" : "")
					+ " glitch has occured! Use edge to negate this?"))
				negateGlitchDefender();
			
		if (Game.interactive && !attacker.edged()) { // postEdge
			switch (Game.askOption("Attacker: Do you want to use edge?", "Do not use edge",
					"Push the Limits", "Second Chance")) {
				case 1:
					preEdgeAttacker();
					break;
				case 2:
					postEdgeAttacker();
					break;
			}
		}
		
		if (Game.interactive && !defender.edged()) { // postEdge
			switch (Game.askOption("Defender: Do you want to use edge?", "Do not use edge",
					"Push the Limits", "Second Chance")) {
				case 1:
					preEdgeDefender();
					break;
				case 2:
					postEdgeDefender();
					break;
			}
		}
	}
	
	public void preEdgeAttacker() {
		attacker.preEdge(c1.getAttribute(Attribute.EDG));
		limit = Integer.MAX_VALUE;
	}
	
	public void preEdgeDefender() {
		defender.preEdge(c1.getAttribute(Attribute.EDG));
	}
	
	public void postEdgeAttacker() {
		attacker.postEdge();
	}
	
	public void postEdgeDefender() {
		defender.postEdge();
	}
	
	public void negateGlitchAttacker() {
		attacker.negateGlitch();
	}
	
	public void negateGlitchDefender() {
		defender.negateGlitch();
	}
	
	public boolean same() {
		return grossAttacker() == grossDefender();
	}
	
	public boolean done() {
		return net() > 0;
	}
	
	public int grossAttacker() {
		return attacker.getWins();
	}
	
	public int grossDefender() {
		return defender.getWins();
	}
	
	public int net() {
		return Math.max(Math.min(grossAttacker(), limit) - grossDefender(), 0);
	}
	
	public String toString() {
		return attacker.toString() + (limit != Integer.MAX_VALUE ? " [" + limit + "] vs " : " vs ")
				+ defender.toString();
	}
}
