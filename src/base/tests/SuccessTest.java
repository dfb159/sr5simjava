package base.tests;

import java.util.ArrayList;
import java.util.Arrays;

import base.Attribute;
import base.Character;
import base.Game;
import base.Skill;
import base.SkillSpecialty;

public class SuccessTest {
	
	public static class Builder {
		private Character					c;
		private int							d, limit, threshold;
		private ArrayList<SkillSpecialty>	special	= new ArrayList<SkillSpecialty>();
		
		public Builder(Character c) {
			this.c = c;
		}

		public Builder add(int bonus) {
			d += bonus;
			return this;
		}
		
		public Builder add(Attribute attr) {
			add(c.getAttribute(attr));
			return this;
		}
		
		public Builder add(Skill skill) {
			add(c.getSkill(skill));
			special.addAll(Arrays.asList(c.getSpecials(skill)));
			return this;
		}
		
		public Builder add(Attribute attr, Skill skill) {
			add(attr);
			add(skill);
			return this;
		}
		
		public Builder add(Attribute attr1, Attribute attr2) {
			add(attr1);
			add(attr2);
			return this;
		}
		
		public Builder mali() {
			this.d -= c.mali();
			return this;
		}
		
		public Builder limit(int limit) {
			this.limit = limit;
			return this;
		}
		
		public Builder threshold(int threshold) {
			this.threshold = threshold;
			return this;
		}
		
		public SuccessTest build() {
			return new SuccessTest(c, d, limit, threshold, (SkillSpecialty[]) special.toArray());
		}
	}
	
	public Dice			test;
	private Character	c;
	private int			limit;
	private int			threshold;
	
	public SuccessTest(Character c, Skill skill, Attribute attr, int lim, int threshold) {
		this(c, skill, attr, 0, lim, threshold);
	}
	
	public SuccessTest(Character c, Skill skill, Attribute attr, int bonus, int lim,
			int threshold) {
		this(c, c.getSkill(skill) + c.getAttribute(attr) + bonus, lim, threshold,
				c.getSpecials(skill));
	}
	
	public SuccessTest(Character c, Attribute attr1, Attribute attr2, int lim, int threshold) {
		this(c, attr1, attr2, 0, lim, threshold);
	}
	
	public SuccessTest(Character c, Attribute attr1, Attribute attr2, int bonus, int lim,
			int threshold) {
		this(c, c.getAttribute(attr1) + c.getAttribute(attr2) + bonus, lim, threshold);
	}
	
	public SuccessTest(Character c, int dice, int lim, int threshold) {
		this(c, dice, lim, threshold, null);
	}
	
	public SuccessTest(Character c, int dice, int lim, int threshold, SkillSpecialty[] specials) {
		this.threshold = threshold;
		this.c = c;
		
		boolean edge = false;
		
		if (Game.interactive && specials != null) {
			Game.msg("Specialties:");
			for (int i = 0; i < specials.length; i++) {
				Game.msg(" - " + specials[i].toString());
			}
			if (Game.askBoolean("Any valid specialty?"))
				dice += 2;
		}
		
		if (Game.interactive) // preEdge
			edge = Game.askBoolean("Push the limits?");
		
		this.limit = (edge || lim <= 0) ? Integer.MAX_VALUE : lim;
		if (edge)
			dice += c.getAttribute(Attribute.EDG);
		test = new Dice(dice, edge);
		
		if (Game.interactive && !test.edged() && test.isGlitch()) // negate critical
			if (Game.askBoolean("A " + (test.isCritical() ? "critical" : "")
					+ " glitch has occured! Use edge to negate this?"))
				negateGlitch();
			
		if (Game.interactive && !test.edged()) { // postEdge
			switch (Game.askOption("Do you want to use edge?", "Do not use edge", "Push the Limits",
					"Second Chance")) {
				case 1:
					preEdge();
					break;
				case 2:
					postEdge();
					break;
			}
		}
	}
	
	public void preEdge() {
		test.preEdge(c.getAttribute(Attribute.EDG));
		limit = Integer.MAX_VALUE;
	}
	
	public void postEdge() {
		test.postEdge();
	}
	
	public void negateGlitch() {
		test.negateGlitch();
	}
	
	public boolean done() {
		return net() > 0;
	}
	
	public int gross() {
		return test.getWins();
	}
	
	public int net() {
		return Math.max(Math.min(test.getWins(), limit) - threshold, 0);
	}
	
	public String toString() {
		return test.toString() + " (" + threshold
				+ (limit != Integer.MAX_VALUE ? ")[" + limit + "]" : ")");
	}
}
