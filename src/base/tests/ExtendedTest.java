package base.tests;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import base.Attribute;
import base.Character;
import base.Game;
import base.Skill;
import base.SkillSpecialty;

public class ExtendedTest {
	
	public static class Builder {
		
		private Character					c;
		private int							d, limit, threshold;
		private String						base	= "3s";
		private ArrayList<SkillSpecialty>	special	= new ArrayList<SkillSpecialty>();
		
		public Builder(Character c) {
			this.c = c;
		}
		
		public Builder add(Attribute attr) {
			add(c.getAttribute(attr));
			return this;
		}
		
		public Builder add(Skill skill) {
			add(c.getSkill(skill));
			special.addAll(Arrays.asList(c.getSpecials(skill)));
			return this;
		}
		
		public Builder add(int bonus) {
			this.d += bonus;
			return this;
		}
		
		public Builder add(Attribute attr1, Attribute attr2) {
			add(attr1);
			add(attr2);
			return this;
		}
		
		public Builder add(Attribute attr, Skill skill) {
			add(attr);
			add(skill);
			return this;
		}
		
		public Builder mali() {
			this.d -= c.mali();
			return this;
		}
		
		public Builder limit(int limit) {
			this.limit = limit;
			return this;
		}
		
		public Builder threshold(int threshold) {
			this.threshold = threshold;
			return this;
		}
		
		public Builder time(String baseTime) {
			this.base = baseTime;
			return this;
		}
		
		public ExtendedTest build() {
			return new ExtendedTest(c, d, limit, threshold, base,
					(SkillSpecialty[]) special.toArray());
		}
	}
	
	public Dice			test;
	private Character	c;
	private int			limit;
	private int			threshold;
	private String		baseTime;
	private double		multiplier	= 0;
	private int			dice;
	private int			hits		= 0;
	private boolean		ignoreLimit;
	private boolean		end			= false;
	
	public ExtendedTest(Character c, Skill skill, Attribute attr, int limit, int threshold,
			String baseTime) {
		this(c, c.getSkill(skill) + c.getAttribute(attr), limit, threshold, baseTime,
				c.getSpecials(skill));
	}
	
	public ExtendedTest(Character c, Attribute attr1, Attribute attr2, int limit, int threshold,
			String baseTime) {
		this(c, c.getAttribute(attr1) + c.getAttribute(attr2), limit, threshold, baseTime, null);
	}
	
	public ExtendedTest(Character c, int dice, int limit, int threshold, String baseTime,
			SkillSpecialty[] specials) {
		this.c = c;
		this.dice = dice;
		this.limit = limit <= 0 ? Integer.MAX_VALUE : limit;
		this.threshold = threshold;
		this.baseTime = baseTime;
		
		if (Game.interactive && specials != null) {
			Game.msg("Specialties:");
			for (int i = 0; i < specials.length; i++) {
				Game.msg(" - " + specials[i].toString());
			}
			if (Game.askBoolean("Any valid specialty?"))
				dice += 2;
		}
		
		round();
	}
	
	public void fullRound() {
		while (!end)
			round();
	}
	
	public void round() {
		ignoreLimit = false;
		if (Game.interactive) // preEdge
			ignoreLimit = Game.askBoolean("Round " + multiplier + ": Push the limits?");
		
		test = new Dice(dice + (ignoreLimit ? c.getAttribute(Attribute.EDG) : 0), ignoreLimit);
		
		if (Game.interactive && !test.edged() && test.isGlitch()) // negate critical
			if (Game.askBoolean("A " + (test.isCritical() ? "critical" : "")
					+ " glitch has occured! Use edge to negate this?"))
				negateGlitch();
			
		if (Game.interactive && !test.edged()) { // postEdge
			switch (Game.askOption("Do you want to use edge?", "Do not use edge", "Push the Limits",
					"Second Chance")) {
				case 1:
					preEdge();
					break;
				case 2:
					postEdge();
					break;
			}
		}
		
		hits += ignoreLimit ? test.getWins() : Math.min(test.getWins(), limit);
		dice -= 1;
		if (test.isGlitch())
			glitch();
		if (test.isCritical() || dice <= 0)
			end = true;
		if (done()) {
			end = true;
			multiplier += 1. / net();
		} else {
			multiplier += 1;
		}
	}
	
	public void preEdge() {
		test.preEdge(c.getAttribute(Attribute.EDG));
		ignoreLimit = true;
	}
	
	public void postEdge() {
		test.postEdge();
	}
	
	public void negateGlitch() {
		test.negateGlitch();
	}
	
	public void glitch() {
		hits -= new Random().nextInt(6) + 1;
	}
	
	public boolean done() {
		return net() > 0;
	}
	
	public String time() {
		return multiplier + " * " + baseTime;
	}
	
	public int net() {
		return Math.max(hits - threshold, 0);
	}
	
	public String toString() {
		return hits + "/" + threshold + " [" + time() + "]";
	}
}
