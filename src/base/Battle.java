package base;

import java.util.ArrayList;

public class Battle extends TimeEffect {
	
	ArrayList<BattleChar>	chars	= new ArrayList<BattleChar>();
	boolean					running	= true;
	
	public Battle() {
		super(Game.time.getSeconds(), -1000); // everything else first
	}
	
	@Override
	public void activate() { // Declare new battle round
		if (!running)
			return;
		Game.time.add(Time.Round);
		for (BattleChar c : chars)
			c.round();
		this.add(Time.Round);
		Game.effect(this);
	}
	
	public void end() {
		this.running = false;
	}
	
	public void addParticipant(Character newbie) {
		BattleChar c = new BattleChar(newbie);
		chars.add(c);
		Game.effect(c);
	}
}
