package base;

import static base.Skill.*;

public class SkillSpecialty {
	
	public static final SkillSpecialty
	NullSpecial = new SkillSpecialty(NullSkill),
	
	Bow = new SkillSpecialty(Archery), Crossbow = new SkillSpecialty(Archery), NonStandardAmmuniton = new SkillSpecialty(Archery), Slingshot = new SkillSpecialty(Archery),
	AssaultRifles = new SkillSpecialty(Automatics), AutomaticCyberImplants = new SkillSpecialty(Automatics), MachinePistols = new SkillSpecialty(Automatics), SubmachineGuns = new SkillSpecialty(Automatics),
	Axes = new SkillSpecialty(Blades), Knives = new SkillSpecialty(Blades), Swords = new SkillSpecialty(Blades), BladeParrying = new SkillSpecialty(Blades),
	Batons = new SkillSpecialty(Clubs), Hammers = new SkillSpecialty(Clubs), Saps = new SkillSpecialty(Clubs), Staves = new SkillSpecialty(Clubs), ClubParrying = new SkillSpecialty(Clubs),
	AssaultCannons = new SkillSpecialty(HeavyWeapons), GrenadeLaunchers = new SkillSpecialty(HeavyWeapons), GuidedMissles = new SkillSpecialty(HeavyWeapons), MachineGuns = new SkillSpecialty(HeavyWeapons), RocketLaunchers = new SkillSpecialty(HeavyWeapons),
	ExtendedRangeShots = new SkillSpecialty(Longarms), LongRangeShots = new SkillSpecialty(Longarms), Shotguns = new SkillSpecialty(Longarms), SniperRifles = new SkillSpecialty(Longarms),
	Holdouts = new SkillSpecialty(Pistols), Revolvers = new SkillSpecialty(Pistols), SemiAutomatics = new SkillSpecialty(Pistols), Tasers = new SkillSpecialty(Pistols),
	Aerodynamics = new SkillSpecialty(ThrowingWeapons), ThrowingBlades = new SkillSpecialty(ThrowingWeapons), NonAerodynamics = new SkillSpecialty(ThrowingWeapons),
	UnarmedBlocking = new SkillSpecialty(UnarmedCombat), UnarmedCyberImplants = new SkillSpecialty(UnarmedCombat), SubduingCombat = new SkillSpecialty(UnarmedCombat), MartialArts = new SkillSpecialty(UnarmedCombat), // TODO martial arts
	Camouflage = new SkillSpecialty(Disguise), Cosmetic = new SkillSpecialty(Disguise), Theatrical = new SkillSpecialty(Disguise), Video = new SkillSpecialty(Disguise),
	ControlledHyperventilation = new SkillSpecialty(Diving), LiquidBrething = new SkillSpecialty(Diving), MixedGasBreathing = new SkillSpecialty(Diving), OxygenExtractionBreathing = new SkillSpecialty(Diving), SCUBABreathing = new SkillSpecialty(Diving), Arctic = new SkillSpecialty(Diving), Cave = new SkillSpecialty(Diving), Commercial = new SkillSpecialty(Diving), Millitary = new SkillSpecialty(Diving), // TODO Diving conditions
	Cuffs = new SkillSpecialty(EscapeArtist), Ropes = new SkillSpecialty(EscapeArtist), ZipTies = new SkillSpecialty(EscapeArtist), Contortionism = new SkillSpecialty(EscapeArtist),
	BASEJumping = new SkillSpecialty(FreeFall), BreakFall = new SkillSpecialty(FreeFall), Bungee = new SkillSpecialty(FreeFall), HALO = new SkillSpecialty(FreeFall), LowAltitude = new SkillSpecialty(FreeFall), Parachute = new SkillSpecialty(FreeFall), StaticLine = new SkillSpecialty(FreeFall), Wingsuit = new SkillSpecialty(FreeFall), Zipline = new SkillSpecialty(FreeFall),
	Balance = new SkillSpecialty(Gymnastics), Climbing = new SkillSpecialty(Gymnastics), Dance = new SkillSpecialty(Gymnastics), Leaping = new SkillSpecialty(Gymnastics), Parkour = new SkillSpecialty(Gymnastics), Rolling = new SkillSpecialty(Gymnastics),
	Legerdemain = new SkillSpecialty(Palming), Pickpocket = new SkillSpecialty(Palming), Pilfering = new SkillSpecialty(Palming),
	Hearing = new SkillSpecialty(Perception), Scent = new SkillSpecialty(Perception), Searching = new SkillSpecialty(Perception), Taste = new SkillSpecialty(Perception), Touch = new SkillSpecialty(Perception), Visual = new SkillSpecialty(Perception),
	Distance = new SkillSpecialty(Running), Sprinting = new SkillSpecialty(Running), RunDesert = new SkillSpecialty(Running), RunUrban = new SkillSpecialty(Running), RunWilderness = new SkillSpecialty(Running), // TODO Running terrain
	SneakJungle = new SkillSpecialty(Sneaking), SneakUrban = new SkillSpecialty(Sneaking), SneakDesert = new SkillSpecialty(Sneaking), // TODO sneaking terrain
	SurvivalDesert = new SkillSpecialty(Survival), SurvivalForest = new SkillSpecialty(Survival), SurvivalJungle = new SkillSpecialty(Survival), SurvivalMountain = new SkillSpecialty(Survival), SurvivalPolar = new SkillSpecialty(Survival), SurvivalUrban = new SkillSpecialty(Survival), // TODO survival terrain
	SwimmDash = new SkillSpecialty(Swimming), SwimmLongDistance = new SkillSpecialty(Swimming), 
	TrackDesert = new SkillSpecialty(Tracking), TrackForest = new SkillSpecialty(Tracking), TrackJungle = new SkillSpecialty(Tracking), TrackMountains = new SkillSpecialty(Tracking), TrackPolar = new SkillSpecialty(Tracking), TrackUrban = new SkillSpecialty(Tracking), // TODO tracking terrain
	FastTalking = new SkillSpecialty(Con), Seduction = new SkillSpecialty(Con), 
	Corporate = new SkillSpecialty(Etiquette), HighSociety = new SkillSpecialty(Etiquette), Media = new SkillSpecialty(Etiquette), Mercenary = new SkillSpecialty(Etiquette), Street = new SkillSpecialty(Etiquette), Yakuza = new SkillSpecialty(Etiquette), // TODO cultures and subcultures
	ImpersonateDwarf = new SkillSpecialty(Impersonation), ImpersonateHuman = new SkillSpecialty(Impersonation), ImpersonateElf = new SkillSpecialty(Impersonation), ImpersonateOrk = new SkillSpecialty(Impersonation), ImpersonateTroll = new SkillSpecialty(Impersonation), 
	TeachCombat = new SkillSpecialty(Instruction), TeachLanguage = new SkillSpecialty(Instruction), TeachMagical = new SkillSpecialty(Instruction), TeachResonance = new SkillSpecialty(Instruction), TeachAcademic = new SkillSpecialty(Instruction), TeachStreet = new SkillSpecialty(Instruction), TeachProfessional = new SkillSpecialty(Instruction), TeachInterest = new SkillSpecialty(Instruction), // TODO active category
	Interrogation = new SkillSpecialty(Intimidation), Mental = new SkillSpecialty(Intimidation), Physical = new SkillSpecialty(Intimidation), Torture = new SkillSpecialty(Intimidation), 
	Command = new SkillSpecialty(Leadership), Direct = new SkillSpecialty(Leadership), Inspire = new SkillSpecialty(Leadership), Rally = new SkillSpecialty(Leadership),
	Bargaining = new SkillSpecialty(Negotiation), Contracts = new SkillSpecialty(Negotiation), Diplomacy = new SkillSpecialty(Negotiation),
	Presentation = new SkillSpecialty(Performance), Acting = new SkillSpecialty(Performance), Comedy = new SkillSpecialty(Performance), Musical = new SkillSpecialty(Performance), // TODO performance art
	BrewCommand = new SkillSpecialty(Alchemy), BrewContact = new SkillSpecialty(Alchemy), BrewTime = new SkillSpecialty(Alchemy), BrewCombat = new SkillSpecialty(Alchemy), BrewDetection = new SkillSpecialty(Alchemy), // TODO when and how potions activate
	SpellDesign = new SkillSpecialty(Arcana), FocusDesign = new SkillSpecialty(Arcana), SpiritFormula = new SkillSpecialty(Arcana),
	FocusAnalysis = new SkillSpecialty(Artificing), CraftFokus = new SkillSpecialty(Artificing), // TODO FokusTypes
	AuraReading = new SkillSpecialty(Assensing), AstralSignatures = new SkillSpecialty(Assensing), AssenseMetatype = new SkillSpecialty(Assensing), AssenseSpirit = new SkillSpecialty(Assensing), AssenseFokus = new SkillSpecialty(Assensing), AssenseWard = new SkillSpecialty(Assensing), // TODO aura type
	AstralFokus = new SkillSpecialty(AstralCombat), AstralMagicians = new SkillSpecialty(AstralCombat), AstralSpirits = new SkillSpecialty(AstralCombat), AstralBarriers = new SkillSpecialty(AstralCombat), // TODO weapon fokus types  = new SkillSpeciality(own and opponent)
	BanishAir = new SkillSpecialty(Banishing), BanishMan = new SkillSpecialty(Banishing), BanishFire = new SkillSpecialty(Banishing), BanishEarth = new SkillSpecialty(Banishing), // TODO spirit types
	BindAir = new SkillSpecialty(Binding), BindMan = new SkillSpecialty(Binding), BindFire = new SkillSpecialty(Binding), BindEarth = new SkillSpecialty(Binding), // TODO spirit types
	CounterCombat = new SkillSpecialty(Counterspelling), CounterDetection = new SkillSpecialty(Counterspelling), // TODO spell types
	DisenchantAlchemicalPreparations = new SkillSpecialty(Disenchanting), DisenchantPowerFoci = new SkillSpecialty(Disenchanting), // TODO enchantment type
	RitualAnchored = new SkillSpecialty(RitualSpellcasting), RitualSpell = new SkillSpecialty(RitualSpellcasting), // TODO ritual keyword
	CastCombat = new SkillSpecialty(Spellcasting), CastDetection = new SkillSpecialty(Spellcasting), // TODO spell type
	SummonAir = new SkillSpecialty(Summoning), SummonMan = new SkillSpecialty(Summoning), SummonFire = new SkillSpecialty(Summoning), SummonEarth = new SkillSpecialty(Summoning), // TODO spirit types
	CompileData = new SkillSpecialty(Compiling), CompileMachine = new SkillSpecialty(Compiling), CompileCourier = new SkillSpecialty(Compiling), CompileFaut = new SkillSpecialty(Compiling), CompileCrack = new SkillSpecialty(Compiling), // TODO sprite types
	DecompileData = new SkillSpecialty(Decompiling), DecompileMachine = new SkillSpecialty(Decompiling), DecompileCourier = new SkillSpecialty(Decompiling), DecompileFaut = new SkillSpecialty(Decompiling), DecompileCrack = new SkillSpecialty(Decompiling), // TODO sprite types
	RegisterData = new SkillSpecialty(Registering), RegisterMachine = new SkillSpecialty(Registering), RegisterCourier = new SkillSpecialty(Registering), RegisterFaut = new SkillSpecialty(Registering), RegisterCrack = new SkillSpecialty(Registering), // TODO sprite types
	MechAerospace = new SkillSpecialty(AeronauticsMechanic), MechFixedWing = new SkillSpecialty(AeronauticsMechanic), MechLTA = new SkillSpecialty(AeronauticsMechanic), MechRotaryWing = new SkillSpecialty(AeronauticsMechanic), MechTiltWing = new SkillSpecialty(AeronauticsMechanic), MechVector = new SkillSpecialty(AeronauticsMechanic),
	MechWalker = new SkillSpecialty(AutomotiveMechanic), MechHover = new SkillSpecialty(AutomotiveMechanic), MechTracked = new SkillSpecialty(AutomotiveMechanic), MechWheeled = new SkillSpecialty(AutomotiveMechanic),
	MechElectricalPowerSystems = new SkillSpecialty(IndustrialMechanic), MechHydraulics = new SkillSpecialty(IndustrialMechanic), MechHVAC = new SkillSpecialty(IndustrialMechanic), MechIndustrialRobotics = new SkillSpecialty(IndustrialMechanic), MechStructural = new SkillSpecialty(IndustrialMechanic), MechWelding = new SkillSpecialty(IndustrialMechanic),
	MechMotorboat = new SkillSpecialty(NauticalMechanic), MechSailboat = new SkillSpecialty(NauticalMechanic), MechShip = new SkillSpecialty(NauticalMechanic), MechSubmarine = new SkillSpecialty(NauticalMechanic),
	MechArmor = new SkillSpecialty(Armorer), MechArtillery = new SkillSpecialty(Armorer), MechExplosives = new SkillSpecialty(Armorer), MechFirearms = new SkillSpecialty(Armorer), MechMelee = new SkillSpecialty(Armorer), MechHeavy = new SkillSpecialty(Armorer), MechAssessoires = new SkillSpecialty(Armorer),
	HandleCat = new SkillSpecialty(AnimalHandling), HandleDog = new SkillSpecialty(AnimalHandling), HandleBird = new SkillSpecialty(AnimalHandling), HandleHellHound = new SkillSpecialty(AnimalHandling), HandleHorse = new SkillSpecialty(AnimalHandling), HandleDolphin = new SkillSpecialty(AnimalHandling), AnimalHerding = new SkillSpecialty(AnimalHandling), AnimalRiding = new SkillSpecialty(AnimalHandling), AnimalTraining = new SkillSpecialty(AnimalHandling), // TODO animal type
	Cooking = new SkillSpecialty(Artisan), Sculpting = new SkillSpecialty(Artisan), Drawing = new SkillSpecialty(Artisan), Carpentry = new SkillSpecialty(Artisan), // TODO disciplines
	Bioinformatics = new SkillSpecialty(Biotechnology), Bioware = new SkillSpecialty(Biotechnology), Cloning = new SkillSpecialty(Biotechnology), GeneTherapy = new SkillSpecialty(Biotechnology), VatMaintenance = new SkillSpecialty(Biotechnology),
	Bodyware = new SkillSpecialty(Cybertechnology), Cyberlimbs = new SkillSpecialty(Cybertechnology), Headware = new SkillSpecialty(Cybertechnology), RepairCybertech = new SkillSpecialty(Cybertechnology),
	AnalyticalChemistry = new SkillSpecialty(Chemistry), Biochemistry = new SkillSpecialty(Chemistry), InorganicChemistry = new SkillSpecialty(Chemistry), OrganicChemistry = new SkillSpecialty(Chemistry), PhysicalChemistry = new SkillSpecialty(Chemistry),
	HackDevice = new SkillSpecialty(Hacking), HackFiles = new SkillSpecialty(Hacking), HackHosts = new SkillSpecialty(Hacking), HackPersonas = new SkillSpecialty(Hacking),
	EditFile = new SkillSpecialty(Computer), MatrixSearch = new SkillSpecialty(Computer), MatrixPerception = new SkillSpecialty(Computer), // TODO matrix actions by computer
	DataBombs = new SkillSpecialty(Software), ResonanceSpike = new SkillSpecialty(Software), Tattletail = new SkillSpecialty(Software), Editor = new SkillSpecialty(Software), // TODO complex forms
	Commlinks = new SkillSpecialty(Hardware), Cyberdecks = new SkillSpecialty(Hardware), Smartguns = new SkillSpecialty(Hardware), //TODO hardware types
	CyberDevice = new SkillSpecialty(Cybercombat), CyberGrids = new SkillSpecialty(Cybercombat), CyberIC = new SkillSpecialty(Cybercombat), CyberPersonas = new SkillSpecialty(Cybercombat), CyberSprites = new SkillSpecialty(Cybercombat), // TODO target type
	Communications = new SkillSpecialty(ElectronicWarfare), Encryption = new SkillSpecialty(ElectronicWarfare), Jamming = new SkillSpecialty(ElectronicWarfare), Sensors = new SkillSpecialty(ElectronicWarfare),
	CommercialExplosives = new SkillSpecialty(Demolitions), PlasticExplosives = new SkillSpecialty(Demolitions), ImprovisedExplosives = new SkillSpecialty(Demolitions), Defusing = new SkillSpecialty(Demolitions),
	HealGunshot = new SkillSpecialty(FirstAid), HealResuscitation = new SkillSpecialty(FirstAid), HealBrokenBones = new SkillSpecialty(FirstAid), HealBurns = new SkillSpecialty(FirstAid), // TODO treatment/wound
	Counterfeiting = new SkillSpecialty(Forgery), CredstickForgery = new SkillSpecialty(Forgery), FalseID = new SkillSpecialty(Forgery), ImageDoctoring = new SkillSpecialty(Forgery), PaperForgery = new SkillSpecialty(Forgery),
	CombinationLock = new SkillSpecialty(Locksmith), KeypadLock = new SkillSpecialty(Locksmith), MagLock = new SkillSpecialty(Locksmith), TumblerLock = new SkillSpecialty(Locksmith), VoiceLock = new SkillSpecialty(Locksmith), // TODO lock type
	CosmeticSurgery = new SkillSpecialty(Medicine), ExtendedCare = new SkillSpecialty(Medicine), ImplantSurgery = new SkillSpecialty(Medicine), MagicalHealth = new SkillSpecialty(Medicine), OrganCulture = new SkillSpecialty(Medicine), TraumaSurgery = new SkillSpecialty(Medicine),
	NavARM = new SkillSpecialty(Navigation), NavCelestial = new SkillSpecialty(Navigation), NavCompass = new SkillSpecialty(Navigation), NavMaps = new SkillSpecialty(Navigation), NavGPS = new SkillSpecialty(Navigation), 
	VehicleArtillery = new SkillSpecialty(Gunnery), VehicleBallistic = new SkillSpecialty(Gunnery), VehicleEnergy = new SkillSpecialty(Gunnery), VehicleGuidedMissles = new SkillSpecialty(Gunnery), VehicleRocket = new SkillSpecialty(Gunnery),
	DeepSpace = new SkillSpecialty(PilotAerospace), LaunchCraft = new SkillSpecialty(PilotAerospace), RemoteAero = new SkillSpecialty(PilotAerospace), Semiballistic = new SkillSpecialty(PilotAerospace), Suborbital = new SkillSpecialty(PilotAerospace), 
	FixedWing = new SkillSpecialty(PilotAircraft), LighterThanAir = new SkillSpecialty(PilotAircraft), RemoteAir = new SkillSpecialty(PilotAircraft), RotaryWing = new SkillSpecialty(PilotAircraft), TiltWing = new SkillSpecialty(PilotAircraft), Vector = new SkillSpecialty(PilotAircraft), 
	Biped = new SkillSpecialty(PilotWalker), Multiped = new SkillSpecialty(PilotWalker), Quadroped = new SkillSpecialty(PilotWalker), RemoteWalker = new SkillSpecialty(PilotWalker), 
	Bike = new SkillSpecialty(PilotGroundCraft), Hovercraft = new SkillSpecialty(PilotGroundCraft), RemoteGround = new SkillSpecialty(PilotGroundCraft), Tracked = new SkillSpecialty(PilotGroundCraft), Wheeled = new SkillSpecialty(PilotGroundCraft), 
	Hydrofoil = new SkillSpecialty(PilotWatercraft), Motorboat = new SkillSpecialty(PilotWatercraft), RemoteWater = new SkillSpecialty(PilotWatercraft), Sail = new SkillSpecialty(PilotWatercraft), Ship = new SkillSpecialty(PilotWatercraft), Submarine = new SkillSpecialty(PilotWatercraft);
	
	private Skill node;
	
	public SkillSpecialty(Skill node) {
		this.node = node;
	}
	
	public Skill skill() {
		return node;
	}
}
