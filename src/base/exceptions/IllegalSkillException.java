package base.exceptions;

public class IllegalSkillException extends GameException {
	
	public IllegalSkillException(String str) {
		super(str);
	}
}
