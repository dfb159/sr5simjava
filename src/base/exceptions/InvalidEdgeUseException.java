package base.exceptions;

public class InvalidEdgeUseException extends GameException {

	public InvalidEdgeUseException(String string) {
		super(string);
	}
	
}
