package base.exceptions;

public class GameException extends ArithmeticException {
	
	public GameException(String string) {
		super(string);
	}
	
	public GameException() {
		super();
	}
	
}
