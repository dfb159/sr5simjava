package base.exceptions;

public class IllegalAttributeException extends GameException {

	public IllegalAttributeException(String string) {
		super(string);
	}
	
}
