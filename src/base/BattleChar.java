package base;

import base.exceptions.InvalidEdgeUseException;
import base.tests.Dice;

public class BattleChar extends TimeEffect {
	
	Character	character;
	int			usedInitiative	= 0;
	int[]		initiativeValue	= new int[5];
	boolean		moved			= false;
	boolean		edged			= false;
	boolean		seized			= false;
	boolean		blitz			= false;
	
	public BattleChar(Character character) {
		super(Game.time);
		this.character = character;
	}
	
	@Override
	public int compareTo(Timed other) { // smaller is earlier
		if (this == other)
			return 0;
		if (other instanceof BattleChar)
			return compareTo((BattleChar) other);
		return super.compareTo(other);
	}
	
	/*
	 * 1. seconds 2. moved? 3. seized 4. initiative 5. edge 6. reaction 7. intuition (8. hashing)
	 */
	
	public int compareTo(BattleChar other) { // smaller is earlier
		int value = this.getSeconds() - other.getSeconds(); // smaller first
		if (value != 0)
			return value;
		value = (this.moved ? 1 : 0) - (other.moved ? 1 : 0); // if already moved is later
		if (value != 0)
			return value;
		value = (other.seized ? 1 : 0) - (this.seized ? 1 : 0); // if seized is earlier
		if (value != 0)
			return value;
		value = other.getInitiative() - this.getInitiative(); // bigger first
		if (value != 0)
			return value;
		value = other.character.getAttribute(Attribute.EDG)
				- this.character.getAttribute(Attribute.EDG); // bigger first
		if (value != 0)
			return value;
		value = other.character.getAttribute(Attribute.REA)
				- this.character.getAttribute(Attribute.REA); // bigger first
		if (value != 0)
			return value;
		value = other.character.getAttribute(Attribute.INT)
				- this.character.getAttribute(Attribute.INT); // bigger first
		if (value != 0)
			return value;
		
		return this.hashCode() - other.hashCode(); // random, but consistent
	}
	
	public void round() {
		for (int i = 0; i < initiativeValue.length; i++) {
			initiativeValue[i] = Dice.value(i + 1);
		}
		usedInitiative = 0;
		moved = false;
		edged = false;
		seized = false;
		blitz = false;
		// TODO use edge to get bonus
	}
	
	public void decrease() {
		usedInitiative += 10;
		seized = false;
		moved = true;
	}
	
	@Override
	public int getInitiative() {
		return character.initiative() + getInitiativeValue() - usedInitiative;
	}
	
	public int getInitiativeValue() {
		return initiativeValue[blitz ? 5 : character.initiativeDice() - 1];
	}
	
	public void seize() {
		if (edged)
			throw new InvalidEdgeUseException("Already edged");
		seized = true;
		edged = true;
	}
	
	public void blitz() {
		if (edged)
			throw new InvalidEdgeUseException("Already edged");
		blitz = true;
		edged = true;
	}
	
	@Override
	public int getSeconds() {
		return Game.time.seconds;
	}
	
	@Override
	public void activate() {
		Game.msg(character + " is doing something!");
		moved = true;
		decrease();
		if (getInitiative() > 0) {
			Game.effect(this);
		}
	}
}
