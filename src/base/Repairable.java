package base;

public interface Repairable {

	public void repair(int amount);
	
	public default void breakItem() {
		breakItem(false);
	}
	
	public void breakItem(boolean irreparable);
	
	public ItemState state();
}
