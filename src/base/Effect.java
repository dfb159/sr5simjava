package base;

public interface Effect extends Timed {
	
	public abstract void activate();
	
}
