package base;

public enum Attribute {
	NullAttribute,
	BOD, AGI, REA, STR,
	WIL, LOG, INT, CHA,
	ESS, EDG, MAG, RES,
}
