package base;

import java.util.ArrayList;
import java.util.Collections;
import javax.swing.JOptionPane;

import base.exceptions.IllegalTimeException;
import base.exceptions.WrongManualInputException;

public class Game {
	
	public static boolean				DEBUG		= true;
	public static boolean				ERROR		= true;
	public static boolean				interactive	= false;
	
	public static Time					time		= new Time(0);
	private static ArrayList<Effect>	effects		= new ArrayList<Effect>();
	
	public static void debug(String str) {
		if (DEBUG)
			System.out.println(str);
	}
	
	public static boolean askBoolean(String question) {
		int answer = JOptionPane.showConfirmDialog(null, question);
		switch (answer) {
			case JOptionPane.YES_OPTION:
				return true;
			case JOptionPane.NO_OPTION:
				return false;
			default:
				return false;
		}
	}
	
	public static int askInteger(String question) {
		String answer = JOptionPane.showInputDialog(question);
		try {
			return Integer.parseInt(answer);
		} catch (Exception e) {
			throw new WrongManualInputException();
		}
	}
	
	public static int askOption(String question, String... options) {
		int answer = JOptionPane.showOptionDialog(null, question, "Choose an option",
				JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options,
				options[0]);
		if (answer == JOptionPane.CLOSED_OPTION)
			return 0;
		return answer;
	}
	
	public static void error(String message) {
		if (ERROR)
			System.err.println("ERROR: " + message);
	}
	
	public static void msg(String message) {
		System.out.println(message);
	}
	
	public static void jumpTo(Time time) {
		if (time.compareTo(Game.time) > 0)
			throw new IllegalTimeException();
		Game.time = time;
		timed();
	}
	
	public static void add(Time time) {
		Game.time.add(time);
		timed();
	}
	
	private static void timed() {
		while (!effects.isEmpty() && 0 <= peek().compareTo(Game.time)) {
			next();
		}
	}
	
	public static void next() {
		Effect e = peek();
		remove(e);
		e.activate();
	}
	
	public static Effect peek() {
		return Collections.min(effects);
	}
	
	public static void effect(TimeEffect effect) {
		effects.add(effect);
	}
	
	public static void remove(Effect effect) {
		effects.remove(effect);
	}
	
	public static void main(String[] args) {
		Character a = new Character("Nano", Metatype.Elf);
		a.setSkill(Skill.UnarmedCombat, 3);
		Character b = new Character("Cyber", Metatype.Ork);
		while (askBoolean("Continue?")) {
			a.attack(b);
			System.out.println(a);
			System.out.println(b);
		}
	}
}
