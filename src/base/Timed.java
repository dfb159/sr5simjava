package base;

public interface Timed extends Comparable<Timed> {
	
	public int getSeconds();
	
	public int getInitiative();
	
	@Override
	public default int compareTo(Timed other) { // this < other => negative
		if (this == other)
			return 0;
		int value = this.getSeconds() - other.getSeconds(); // smaller first
		if (value != 0)
			return value;
		value = other.getInitiative() - this.getInitiative(); // bigger first
		if (value != 0)
			return value;
		
		return this.hashCode() - other.hashCode(); // random, but consistent
	}
}
