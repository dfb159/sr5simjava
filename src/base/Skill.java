package base;

import static base.Attribute.*;
import static base.SkillGroup.*;

public enum Skill {
	NullSkill(NullGroup, NullAttribute, false),
	
	Con(Acting, CHA, true),
	Impersonation(Acting, CHA, true),
	Performance(Acting, CHA, true),
	
	Gymnastics(Athletics, AGI, true),
	Running(Athletics, STR, true),
	Swimming(Athletics, STR, true),
	
	Biotechnology(Biotech, LOG, false),
	Cybertechnology(Biotech, LOG, false),
	FirstAid(Biotech, LOG, true),
	Medicine(Biotech, LOG, false),
	
	Blades(CloseCombat, AGI, true),
	Clubs(CloseCombat, AGI, true),
	UnarmedCombat(CloseCombat, AGI, true),
	
	Banishing(Conjuring, MAG, false),
	Binding(Conjuring, MAG, false),
	Summoning(Conjuring, MAG, false),
	
	Cybercombat(Cracking, LOG, true),
	ElectronicWarfare(Cracking, LOG, false),
	Hacking(Cracking, LOG, true),
	
	Computer(Electronics, LOG, true),
	Hardware(Electronics, LOG, false),
	Software(Electronics, LOG, false),
	
	Alchemy(Enchanting, MAG, false),
	Artificing(Enchanting, MAG, false),
	Disenchanting(Enchanting, MAG, false),
	
	AeronauticsMechanic(Engineering, LOG, false),
	AutomotiveMechanic(Engineering, LOG, false),
	IndustrialMechanic(Engineering, LOG, false),
	NauticalMechanic(Engineering, LOG, false),
	
	Automatics(Firearms, AGI, true),
	Longarms(Firearms, AGI, true),
	Pistols(Firearms, AGI, true),
	
	Etiquette(Influence, CHA, true),
	Leadership(Influence, CHA, true),
	Negotiation(Influence, CHA, true),
	
	Navigation(Outdoors, INT, true),
	Survival(Outdoors, WIL, true),
	Tracking(Outdoors, INT, true),
	
	Counterspelling(Sorcery, MAG, false),
	RitualSpellcasting(Sorcery, MAG, false),
	Spellcasting(Sorcery, MAG, false),
	
	Disguise(Stealth, INT, true),
	Palming(Stealth, AGI, true),
	Sneaking(Stealth, AGI, true),
	
	Compiling(Tasking, RES, false),
	Decompiling(Tasking, RES, false),
	Registering(Tasking, RES, false),

	PilotAerospace(NullGroup, REA, false),
	PilotAircraft(NullGroup, REA, false),
	PilotWalker(NullGroup, REA, false),
	PilotGroundCraft(NullGroup, REA, true),
	PilotWatercraft(NullGroup, REA, true),
	Gunnery(NullGroup, AGI, true),
	
	Archery(NullGroup, AGI, true),
	HeavyWeapons(NullGroup, AGI, true),
	ThrowingWeapons(NullGroup, AGI, true),

	AstralCombat(NullGroup, WIL, false),
	Assensing(NullGroup, INT, false),
	Arcana(NullGroup, LOG, false),
	
	EscapeArtist(NullGroup, AGI, true),
	Locksmith(NullGroup, AGI, false),
	Diving(NullGroup, BOD, true),
	FreeFall(NullGroup, BOD, true),
	Instruction(NullGroup, CHA, true),
	Intimidation(NullGroup, CHA, true),
	AnimalHandling(NullGroup, CHA, true),
	Artisan(NullGroup, INT, false),
	Armorer(NullGroup, LOG, true),
	Chemistry(NullGroup, LOG, false),
	Demolitions(NullGroup, LOG, true),
	Forgery(NullGroup, LOG, true),
	Perception(NullGroup, INT, true),
	
	ExoticMeleeWeapon(NullGroup, AGI, false),
	ExoticRangedWeapon(NullGroup, AGI, false),
	PilotExoticVehicle(NullGroup, AGI, false),

	Language(NullGroup, INT, true),
	InterestsKnowledge(NullGroup, INT, false),
	StreetKnowledge(NullGroup, INT, false),
	AcademicKnowledge(NullGroup, LOG, false),
	ProfessionalKnowledge(NullGroup, LOG, false);

	// TODO Knowledgeskills != normal Skills
	// TODO ExpertiseSkills -> extra Dice
	
	SkillGroup group;
	Attribute attr;
	boolean def;
	
	Skill(SkillGroup group, Attribute defaultAttribute, boolean def) {
		this.group = group;
		this.attr = defaultAttribute;
		this.def = def;
	}
	
	public SkillGroup group() {
		return group;
	}
	
	public Attribute attribute() {
		return attr;
	}
	
	public boolean canDefault() {
		return def;
	}
}
