package base;

public class Time implements Timed {
	
	public static final Time	NullTime	= new Time(0);
	public static final Time	Round		= new Time(3);
	public static final Time	Second		= new Time(1);
	public static final Time	Minute		= new Time(60);
	public static final Time	Hour		= new Time(60 * 60);
	public static final Time	Day			= new Time(60 * 60 * 24);
	public static final Time	Week		= new Time(60 * 60 * 24 * 7);
	public static final Time	Month		= new Time(60 * 60 * 24 * 30);
	public static final Time	Year		= new Time(60 * 60 * 24 * 365);
	
	int							seconds, initiative;
	
	public Time(Time time) {
		this(time.seconds, time.initiative);
	}
	
	public Time(int seconds) {
		this(seconds, 0);
	}
	
	public Time(int seconds, int initiative) {
		this.seconds = seconds;
		this.initiative = initiative;
	}
	
	public void set(Time time) {
		this.seconds = time.seconds;
		this.initiative = time.initiative;
	}
	
	public void add(Time time) {
		this.seconds += time.seconds;
		this.initiative += time.initiative;
	}
	
	public int getSeconds() {
		return seconds;
	}
	
	public int getInitiative() {
		return initiative;
	}
	
	public String toString() {
		return seconds + "s" + (initiative >= 0 ? "(" + initiative + ")" : "");
	}
	
}
