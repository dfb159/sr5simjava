package base;

public enum ItemState {
	New, Damaged, Broken, Irrepairable;
}
