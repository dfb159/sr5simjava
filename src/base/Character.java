package base;

import java.util.ArrayList;
import java.util.EnumMap;

import armor.Armor;
import base.elemental.ElementalType;
import base.exceptions.MatrixOfflineException;
import base.tests.OpposedTest;
import base.tests.SuccessTest;
import weapons.Weapon;
import weapons.melee.Unarmed;

import static base.Attribute.*;
import static base.MatrixAttribute.*;
import static base.State.*;
import static base.DamageType.*;

public class Character {
	
	public String							name, alias, sex, age, eyes, height, weight, skin, hair,
			description, background, concept, notes, gamenotes, playername;
	public Metatype							metatype;
	
	private int								HP_phys;
	private int								HP_stun;
	private State							state			= OFFLINE;
	public Persona							persona			= null;
	
	public Weapon							weapon			= new Unarmed(this);
	private ArrayList<Armor>				armor			= new ArrayList<Armor>();
	private int								carryWeight		= 0;
	private int								carryMod		= 0;
	
	public double							essenceLost		= 0.0;
	public EnumMap<Attribute, Integer>		attributes		= new EnumMap<Attribute, Integer>(
			Attribute.class);
	public EnumMap<Skill, Integer>			skills			= new EnumMap<Skill, Integer>(
			Skill.class);
	public EnumMap<SkillGroup, Integer>		skillGroups		= new EnumMap<SkillGroup, Integer>(
			SkillGroup.class);
	public ArrayList<SkillSpecialty>		skillSpecials	= new ArrayList<SkillSpecialty>();
	public EnumMap<ElementalType, Integer>	resistance		= new EnumMap<ElementalType, Integer>(
			ElementalType.class);
	
	public Character(String name, String alias, Metatype type, String sex, String age, String eyes,
			String height, String weight, String skin, String hair, String description,
			String background, String concept, String notes, String gamenotes, String playername) {
		this.name = name;
		this.alias = alias;
		this.metatype = type;
		this.sex = sex;
		this.age = age;
		this.eyes = eyes;
		this.height = height;
		this.weight = weight;
		this.skin = skin;
		this.hair = hair;
		this.description = description;
		this.background = background;
		this.concept = concept;
		this.notes = notes;
		this.gamenotes = gamenotes;
		this.playername = playername;
	}
	
	public Character(String name, Metatype type) {
		this(name, name, type, "", "", "", "", "", "", "", "", "", "", "", "", "");
	}
	
	public void setEssence(double lost) {
		this.essenceLost = lost;
	}
	
	public double getEssence() {
		return getAttribute(ESS) - essenceLost;
	}
	
	public int getHP_physical() {
		return HP_phys;
	}
	
	public int getHP_stun() {
		return HP_stun;
	}
	
	public void setAttribute(Attribute attr, int level) {
		attributes.put(attr, level);
	}
	
	public int getAttribute(Attribute attr) {
		return (attributes.containsKey(attr) ? attributes.get(attr) : 0)
				+ metatype.baseStats.get(attr);
	}
	
	public void setSkill(Skill skill, int level) {
		skills.put(skill, level);
	}
	
	public int getSkill(Skill skill) {
		return skills.containsKey(skill) ? skills.get(skill) : -1;
	}
	
	public void setResistance(ElementalType type, int value) {
		resistance.put(type, value);
	}
	
	public int getResistance(ElementalType type) {
		return resistance.containsKey(type) ? resistance.get(type) : 0;
	}
	
	public int getMatrixAttribute(MatrixAttribute attr) {
		if (state == OFFLINE)
			throw new MatrixOfflineException();
		return persona.getMatrixAttribute(attr);
	}
	
	public int getLimit(Limit limit) {
		switch (limit) {
			case BODY:
				return (getAttribute(STR) * 2 + getAttribute(BOD) + getAttribute(REA) + 2) / 3
						+ getCarryModifier();
			case MENTAL:
				return (getAttribute(LOG) * 2 + getAttribute(INT) + getAttribute(WIL) + 2) / 3;
			case SOCIAL:
				return (getAttribute(CHA) * 2 + getAttribute(WIL) + getAttribute(ESS) + 2) / 3;
			default:
				return 0;
		}
	}
	
	public int getArmor(ElementalType element) {
		return armor.stream().mapToInt((Armor a) -> a.armor(element)).sum();
	}
	
	public void addArmor(Armor armor) {
		if (!this.armor.contains(armor))
			this.armor.add(armor);
	}
	
	public void removeArmor(Armor armor) {
		if (this.armor.contains(armor))
			this.armor.remove(armor);
	}
	
	public void reduceArmor(int amount) {
		for (Armor a : armor) {
			if (amount <= 0)
				return;
			int reduction = Math.min(a.armor(), amount);
			a.reduce(reduction);
			amount -= reduction;
		}
	}
	
	public SkillSpecialty[] getSpecials(Skill skill) {
		return (SkillSpecialty[]) skillSpecials.parallelStream()
				.filter((SkillSpecialty spec) -> spec.skill() == skill).toArray();
	}
	
	public boolean ko() {
		return HP_phys >= physicalKO() || HP_stun >= mentalKO();
	}
	
	public boolean dead() {
		return HP_phys > physicalDead();
	}
	
	public int physicalKO() {
		return (getAttribute(BOD) + 1) / 2 + 8;
	}
	
	public int physicalDead() {
		return physicalKO() + getAttribute(BOD);
	}
	
	public int mentalKO() {
		return (getAttribute(WIL) + 1) / 2 + 8;
	}
	
	public int mali() {
		return HP_phys / 3 + HP_stun / 3;
	}
	
	public String status() {
		if (dead())
			return "Dead";
		if (ko())
			return "KO";
		if (HP_phys == 0 && HP_stun == 0)
			return "Healthy";
		return "Wounded (" + mali() + ")";
	}
	
	public void damage(int dmg, DamageType type) {
		if (dmg <= 0)
			return;
		switch (type) {
			case PHYSICAL:
				HP_phys += dmg;
				break;
			case STUN:
				HP_stun += dmg;
				break;
		}
		int overflow = HP_stun - mentalKO();
		if (overflow > 0) {
			HP_phys += overflow / 2;
			HP_stun = mentalKO();
		}
	}
	
	public void resist(int dmg, DamageType type, ElementalType element, int armorPen) {
		int armor = getArmor(element) - armorPen;
		int bod = getAttribute(BOD);
		if (dmg < armor)
			type = STUN;
		SuccessTest test = new SuccessTest(this, armor + bod, -1, 0);
		int damage = dmg - test.gross();
		damage(damage, type);
	}
	
	public void coldEffect() {
		for (Armor a : armor) {
			SuccessTest test = new SuccessTest.Builder(this).add(a.armor()).build();
			if (test.test.isCritical())
				a.breakItem(true); // TODO break the armor irrepairable in a spectacular way
			else if (test.test.isGlitch())
				a.breakItem(true); // break the armor irrepairable
			else if (test.gross() == 0)
				a.breakItem(); // break the armor
		}
	}
	
	public int initiative() {
		switch (state) {
			case OFFLINE:
			case AR:
			case RIGGER:
				return getAttribute(REA) + getAttribute(INT) - mali();
			case VRCold:
			case VRHot:
				return getMatrixAttribute(Data) + getAttribute(INT) - mali();
			case ASTRAL:
				return getAttribute(INT) * 2 - mali();
			default:
				return 0;
		}
	}
	
	public int initiativeDice() { // TODO bonus
		switch (state) {
			case OFFLINE:
			case AR:
				return 1;
			case ASTRAL:
				return 2;
			case VRCold:
				return 3;
			case VRHot:
			case RIGGER:
				return 4;
		}
		return 1;
	}
	
	public void setWeapon(Weapon weapon) {
		this.weapon = weapon;
	}
	
	public void attack(Character other) {
		OpposedTest test = new OpposedTest.Builder(this, other)
				.addAtk(weapon.skill(), weapon.attribute()).addDef(REA, INT)
				.limit(weapon.accuracy()).mali().build(); // TODO umweltmodifikatoren
		if (test.attacker.isCritical())
			// TODO critical miss
			return;
		if (test.grossAttacker() <= 0)
			return;
		if (test.same()) { // graze
		} else if (test.done()) {
			// TODO elemental damage
			other.resist(weapon.damage(), weapon.type(), weapon.element(), weapon.penetration());
		}
	}
	
	public SuccessTest composure(int threshold) {
		return new SuccessTest(this, CHA, WIL, -mali(), -1, threshold);
	}
	
	public OpposedTest judgeIntentions(Character other) {
		return new OpposedTest(this, CHA, INT, -mali(), -1, other, WIL, CHA);
	}
	
	public boolean lift(int weight) { // maybe a weight class?
		int base = this.getAttribute(STR) * 15;
		if (base >= weight)
			return true;
		SuccessTest test = new SuccessTest(this, BOD, STR, -mali(), -1, 0);
		if (test.test.isCritical())
			return false;
		if (test.test.isGlitch())
			return base + 5 * test.gross() >= weight;
		return base + 15 * test.gross() >= weight;
	}
	
	public boolean liftHigh(int weight) { // maybe a weight class?
		int base = this.getAttribute(STR) * 5;
		if (base >= weight)
			return true;
		SuccessTest test = new SuccessTest(this, BOD, STR, -mali(), -1, 0);
		if (test.test.isCritical())
			return false;
		if (test.test.isGlitch())
			return base + 2 * test.gross() >= weight;
		return base + 5 * test.gross() >= weight;
	}
	
	public int getCarryModifier() {
		return carryMod;
	}
	
	public void addCarryWeight(int weight) { // maybe a weight class?
		this.carryWeight += weight;
		int base = this.getAttribute(STR) * 10;
		if (base >= carryWeight)
			this.carryMod = 0;
		SuccessTest test = new SuccessTest(this, BOD, STR, -1, 0);
		this.carryMod = Math.min((base + 10 * test.gross() - carryWeight + 9) / 10, 0);
	}
	
	public SuccessTest memory(int threshold, int bonus) {
		return new SuccessTest(this, getAttribute(LOG) + getAttribute(WIL) + bonus - mali(), -1,
				threshold);
	}
	
	public String toString() {
		return alias + " (" + name + ") " + status();
	}
}
