package base;

public enum SkillGroup {
	NullGroup,
	Acting, Athletics, Biotech, CloseCombat, Conjuring, Cracking, Electronics, Enchanting, Firearms, Influence, Engineering, Outdoors, Sorcery, Stealth, Tasking;
}
