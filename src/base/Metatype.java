package base;

import java.util.EnumMap;

public enum Metatype {
	Human(1, 6, 1, 6, 1, 6, 1, 6, 1, 6, 1, 6, 1, 6, 1, 6, 2, 7),
	Elf(1, 6, 2, 7, 1, 6, 1, 6, 1, 6, 1, 6, 1, 6, 3, 8, 1, 6), 
	Dwarf(3, 8, 1, 6, 1, 5, 3, 8, 2, 8, 1, 6, 1, 6, 1, 6, 1, 6), 
	Ork(4, 9, 1, 6, 1, 6, 3, 8, 1, 6, 1, 5, 1, 6, 1, 5, 1, 6), 
	Troll(5, 10, 1, 5, 1, 6, 5, 10, 1, 6, 1, 5, 1, 5, 1, 4, 1, 6);

	EnumMap<Attribute, Integer> baseStats = new EnumMap<Attribute, Integer>(Attribute.class);
	EnumMap<Attribute, Integer> maxStats = new EnumMap<Attribute, Integer>(Attribute.class);
	
	private Metatype(int bbody, int mbody, int bagi, int magi, int brea, int mrea, int bstr,
			int mstr, int bwil, int mwil, int blog, int mlog, int bint, int mint, int bcha,
			int mcha, int bedg, int medg) {
		baseStats.put(Attribute.BOD, bbody);
		baseStats.put(Attribute.AGI, bagi);
		baseStats.put(Attribute.REA, brea);
		baseStats.put(Attribute.STR, bstr);
		baseStats.put(Attribute.WIL, bwil);
		baseStats.put(Attribute.LOG, blog);
		baseStats.put(Attribute.INT, bint);
		baseStats.put(Attribute.CHA, bcha);
		baseStats.put(Attribute.ESS, 6);
		baseStats.put(Attribute.EDG, bedg);
		baseStats.put(Attribute.MAG, 0);
		baseStats.put(Attribute.RES, 0);
		maxStats.put(Attribute.BOD, mbody);
		maxStats.put(Attribute.AGI, magi);
		maxStats.put(Attribute.REA, mrea);
		maxStats.put(Attribute.STR, mstr);
		maxStats.put(Attribute.WIL, mwil);
		maxStats.put(Attribute.LOG, mlog);
		maxStats.put(Attribute.INT, mint);
		maxStats.put(Attribute.CHA, mcha);
		maxStats.put(Attribute.ESS, 6);
		maxStats.put(Attribute.EDG, medg);
		maxStats.put(Attribute.MAG, 6);
		maxStats.put(Attribute.RES, 6);
		
	}
}
