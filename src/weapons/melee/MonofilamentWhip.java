package weapons.melee;

import base.Character;
import base.DamageType;
import base.Skill;

public class MonofilamentWhip extends MeleeWeapon {
	
	boolean online; // TODO implements Matrix
	
	public MonofilamentWhip(Character c) {
		super(c);
		skill = Skill.ExoticMeleeWeapon;
	}
	
	@Override
	public int accuracy() {
		return online ? 7 : 5;
	}
	
	@Override
	public int reach() {
		return super.reach() + 2;
	}
	
	@Override
	public int damage() {
		return 12;
	}
	
	@Override
	public DamageType type() {
		return DamageType.PHYSICAL;
	}
	
	@Override
	public int penetration() {
		return 8;
	}
	
}
