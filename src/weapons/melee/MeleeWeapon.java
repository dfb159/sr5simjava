package weapons.melee;

import weapons.Weapon;

public abstract class MeleeWeapon extends Weapon {

	protected MeleeWeapon(base.Character c) {
		super(c);
	}
	
}
