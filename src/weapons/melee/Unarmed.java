package weapons.melee;

import base.Attribute;
import base.Character;
import base.DamageType;
import base.Limit;
import base.Skill;

public class Unarmed extends MeleeWeapon {
	
	public Unarmed(Character c) {
		super(c);
		skill = Skill.UnarmedCombat;
	}
	
	@Override
	public int accuracy() {
		return user.getLimit(Limit.BODY);
	}
	
	@Override
	public int damage() {
		return user.getAttribute(Attribute.STR);
	}
	
	@Override
	public DamageType type() {
		return DamageType.STUN;
	}
	
	@Override
	public int penetration() {
		return 0;
	}
	
}
