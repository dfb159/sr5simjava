package weapons.melee;

import base.Attribute;
import base.Character;
import base.DamageType;
import base.Limit;
import base.Skill;

public class Knucks extends MeleeWeapon {

	Knucks(Character c) {
		super(c);
		skill = Skill.UnarmedCombat;
	}

	@Override
	public int accuracy() {
		return user.getLimit(Limit.BODY);
	}

	@Override
	public int damage() {
		return user.getAttribute(Attribute.STR) + 1;
	}

	@Override
	public DamageType type() {
		return DamageType.PHYSICAL;
	}

	@Override
	public int penetration() {
		return 0;
	}
	
}
