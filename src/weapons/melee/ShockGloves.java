package weapons.melee;

import base.Character;
import base.DamageType;
import base.Limit;
import base.Skill;
import base.elemental.ElementalType;

public class ShockGloves extends MeleeWeapon {

	protected ShockGloves(Character c) {
		super(c);
		skill = Skill.UnarmedCombat;
	}

	@Override
	public int accuracy() {
		return user.getLimit(Limit.BODY);
	}

	@Override
	public int damage() {
		return 8;
	}

	@Override
	public DamageType type() {
		return DamageType.STUN;
	}

	@Override
	public int penetration() {
		return 5;
	}
	
	@Override
	public ElementalType element() {
		return ElementalType.Electric;
	}
	
}
