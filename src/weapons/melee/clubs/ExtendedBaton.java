package weapons.melee.clubs;

public class ExtendedBaton extends Clubs {

	public ExtendedBaton(base.Character c) {
		super(c);
		accuracy = 5;
		reach = 1;
		damage = 2;
		penetration = 0;
	}
}
