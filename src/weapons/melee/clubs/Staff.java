package weapons.melee.clubs;

public class Staff extends Clubs {

	public Staff(base.Character c) {
		super(c);
		accuracy = 6;
		reach = 2;
		damage = 3;
		penetration = 0;
	}
}
