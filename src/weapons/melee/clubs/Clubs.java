package weapons.melee.clubs;

import base.Attribute;
import base.DamageType;
import base.Skill;
import weapons.melee.MeleeWeapon;

public abstract class Clubs extends MeleeWeapon {
	
	int accuracy, reach, damage, penetration;

	Clubs(base.Character c) {
		super(c);
		skill = Skill.Clubs;
	}
	
	@Override
	public int accuracy() {
		return accuracy;
	}
	
	@Override
	public int reach() {
		return super.reach() + reach;
	}
	
	@Override
	public DamageType type() {
		return DamageType.PHYSICAL;
	}
	
	@Override
	public int damage() {
		return user.getAttribute(Attribute.STR) + damage;
	}
	
	@Override
	public int penetration() {
		return penetration;
	}
	
}
