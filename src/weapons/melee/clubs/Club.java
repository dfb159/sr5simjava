package weapons.melee.clubs;

public class Club extends Clubs {
	
	public Club(base.Character c) {
		super(c);
		accuracy = 4;
		reach = 1;
		damage = 3;
		penetration = 0;
	}
}
