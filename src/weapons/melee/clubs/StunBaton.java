package weapons.melee.clubs;

import base.DamageType;
import base.elemental.ElementalType;

public class StunBaton extends Clubs {
	
	public StunBaton(base.Character c) {
		super(c);
		accuracy = 4;
		reach = 1;
		damage = 9;
		penetration = 5;
	}
	
	@Override
	public int damage() {
		return damage;
	}
	
	@Override
	public ElementalType element() {
		return ElementalType.Electric;
	}
	
	@Override
	public DamageType type() {
		return DamageType.STUN;
	}
}
