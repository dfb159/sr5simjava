package weapons.melee.clubs;

public class TelescopingStaff extends Clubs {

	public TelescopingStaff(base.Character c) {
		super(c);
		accuracy = 4;
		reach = 2;
		damage = 2;
		penetration = 0;
	}
}
