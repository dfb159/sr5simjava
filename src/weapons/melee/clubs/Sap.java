package weapons.melee.clubs;

public class Sap extends Clubs {

	public Sap(base.Character c) {
		super(c);
		accuracy = 5;
		reach = 0;
		damage = 2;
		penetration = 0;
	}
}
