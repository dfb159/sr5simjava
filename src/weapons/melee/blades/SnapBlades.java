package weapons.melee.blades;

import base.Character;

public class SnapBlades extends Blades {
	
	public SnapBlades(Character c) {
		super(c);
		accuracy = 4;
		reach = 0;
		damage = 2;
		penetration = 2;
	}	
}
