package weapons.melee.blades;

import base.Character;

public class Knife extends Blades {
	
	public Knife(Character c) {
		super(c);
		accuracy = 5;
		reach = 0;
		damage = 1;
		penetration = 1;
	}
	
}
