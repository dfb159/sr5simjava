package weapons.melee.blades;

public class CombatAxe extends Blades {
	
	CombatAxe(base.Character c) {
		super(c);
		accuracy = 4;
		reach = 2;
		damage = 5;
		penetration = 4;
	}
	
}
