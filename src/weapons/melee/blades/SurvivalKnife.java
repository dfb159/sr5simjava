package weapons.melee.blades;

import base.Character;

public class SurvivalKnife extends Blades {
	
	public SurvivalKnife(Character c) {
		super(c);
		accuracy = 5;
		reach = 0;
		damage = 2;
		penetration = 1;
	}
	
}
