package weapons.melee.blades;

import base.Character;

public class Katana extends Blades {
	
	public Katana(Character c) {
		super(c);
		accuracy = 7;
		reach = 1;
		damage = 3;
		penetration = 3;
	}
	
}
