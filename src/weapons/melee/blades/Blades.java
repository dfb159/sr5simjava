package weapons.melee.blades;

import base.Attribute;
import base.DamageType;
import base.Skill;
import weapons.melee.MeleeWeapon;

public abstract class Blades extends MeleeWeapon {
	
	int accuracy, reach, damage, penetration;

	Blades(base.Character c) {
		super(c);
		skill = Skill.Blades;
	}
	
	@Override
	public int accuracy() {
		return accuracy;
	}
	
	@Override
	public int reach() {
		return super.reach() + reach;
	}
	
	@Override
	public DamageType type() {
		return DamageType.PHYSICAL;
	}
	
	@Override
	public int damage() {
		return user.getAttribute(Attribute.STR) + damage;
	}
	
	@Override
	public int penetration() {
		return penetration;
	}
	
}
