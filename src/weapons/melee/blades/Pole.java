package weapons.melee.blades;

import base.Character;

public class Pole extends Blades {
	
	public Pole(Character c) {
		super(c);
		accuracy = 5;
		reach = 3;
		damage = 3;
		penetration = 2;
	}
	
}
