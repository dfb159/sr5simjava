package weapons.melee.blades;

import base.Character;

public class Sword extends Blades {
	
	public Sword(Character c) {
		super(c);
		accuracy = 6;
		reach = 1;
		damage = 3;
		penetration = 2;
	}
	
}
