package weapons.melee.blades;

import base.Character;

public class CombatKnife extends Blades {
	
	public CombatKnife(Character c) {
		super(c);
		accuracy = 6;
		reach = 0;
		damage = 2;
		penetration = 3;
	}
	
}
