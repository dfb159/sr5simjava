package weapons;

import base.Character;
import base.Attribute;
import base.DamageType;
import base.Skill;
import base.elemental.ElementalType;

public abstract class Weapon {
	
	protected Character	user;
	protected Skill		skill;
	protected Attribute	attribute	= Attribute.AGI;
	
	protected Weapon(Character c) {
		this.user = c;
	}
	
	public Weapon() {
		this.user = null;
	}
	
	public void set(Character c) {
		this.user = c;
	}
	
	public Skill skill() {
		return skill;
	}
	
	public Attribute attribute() {
		return attribute;
	}
	
	public abstract int accuracy();
	
	public int reach() {
		switch (user.metatype) {
			case Troll:
				return 1;
			default:
				return 0;
		}
	}
	
	public abstract int damage();
	
	public abstract DamageType type();
	
	public abstract int penetration();
	
	public ElementalType element() {
		return ElementalType.Normal;
	}
	
	// TODO price and availibility
}
