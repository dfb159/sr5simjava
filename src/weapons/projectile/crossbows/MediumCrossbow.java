package weapons.projectile.crossbows;

public class MediumCrossbow extends Crossbow {
	
	public MediumCrossbow(base.Character c) {
		super(c);
		accuracy = 6;
		damage = 7;
		penetration = 2;
	}
}
