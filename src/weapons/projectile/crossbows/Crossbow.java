package weapons.projectile.crossbows;

import base.DamageType;
import weapons.projectile.ProjectileWeapon;

public abstract class Crossbow extends ProjectileWeapon {
	
	int accuracy, damage, penetration;
	
	public Crossbow(base.Character c) {
		super(c);
	}
	
	@Override
	public int accuracy() {
		return accuracy;
	}
	
	@Override
	public int damage() {
		return damage;
	}
	
	@Override
	public DamageType type() {
		return DamageType.PHYSICAL;
	}
	
	@Override
	public int penetration() {
		return penetration;
	}
	
}
