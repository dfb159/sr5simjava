package weapons.projectile.crossbows;

public class LightCrossbow extends Crossbow {
	
	public LightCrossbow(base.Character c) {
		super(c);
		accuracy = 7;
		damage = 5;
		penetration = 1;
	}
}
