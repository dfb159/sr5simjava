package weapons.projectile.crossbows;

public class HeavyCrossbow extends Crossbow {
	
	public HeavyCrossbow(base.Character c) {
		super(c);
		accuracy = 5;
		damage = 10;
		penetration = 3;
	}
}
