package weapons.projectile.throwing;

import base.Attribute;
import base.DamageType;
import base.Limit;
import weapons.projectile.ProjectileWeapon;

public class ThrowingKnife extends ProjectileWeapon {
	
	public ThrowingKnife(base.Character c) {
		super(c);
	}
	
	@Override
	public int accuracy() {
		return user.getLimit(Limit.BODY);
	}
	
	@Override
	public int damage() {
		return user.getAttribute(Attribute.STR) + 1;
	}
	
	@Override
	public DamageType type() {
		return DamageType.PHYSICAL;
	}
	
	@Override
	public int penetration() {
		return 1;
	}
	
}
