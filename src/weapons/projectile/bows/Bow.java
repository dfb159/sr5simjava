package weapons.projectile.bows;

import base.DamageType;
import weapons.projectile.ProjectileWeapon;

public class Bow extends ProjectileWeapon {
	
	int rating;
	
	public Bow(base.Character c, int rating) {
		super(c);
		this.rating = rating;
	}
	
	@Override
	public int accuracy() {
		return 6;
	}
	
	@Override
	public int damage() {
		return rating + 2;
	}
	
	@Override
	public DamageType type() {
		return DamageType.PHYSICAL;
	}
	
	@Override
	public int penetration() {
		return (rating + 3) / 4;
	}
	
}
