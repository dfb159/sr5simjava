package armor;

import base.ItemState;
import base.elemental.ElementalType;
import base.exceptions.CannotReduceNegativeException;
import base.exceptions.CannotRepairNegativeException;
import base.exceptions.ItemIsIrrepairableException;

public class TestArmor extends Armor {
	
	int			maxarmor	= 11;
	int			armor		= 11;			// TODO different elemental types
	ItemState	state		= ItemState.New;
	
	@Override
	public int armor(ElementalType type) {
		return armor;
	}
	
	@Override
	public void reduce(int amount) {
		if (amount <= 0)
			throw new CannotReduceNegativeException();
		armor -= Math.min(armor, amount);
		if (armor == 0) {
			state = ItemState.Broken;
		} else {
			state = ItemState.Damaged;
		}
	}
	
	@Override
	public void repair(int amount) {
		if (amount <= 0)
			throw new CannotRepairNegativeException();
		if (state() == ItemState.Irrepairable)
			throw new ItemIsIrrepairableException();
		armor += Math.min(amount, maxarmor);
		state = ItemState.Damaged;
	}
	
	@Override
	public void breakItem(boolean irrepairable) {
		if (state().compareTo(ItemState.Broken) < 0)
			reduce(armor);
		if (irrepairable)
			state = ItemState.Irrepairable;
	}
	
	@Override
	public ItemState state() {
		return state;
	}
	
}
