package armor;

import base.Repairable;
import base.elemental.ElementalType;

public abstract class Armor implements Repairable {
	
	public int armor() {
		return armor(ElementalType.Normal);
	}
	
	public abstract int armor(ElementalType type);
	
	public abstract void reduce(int amount);
}
